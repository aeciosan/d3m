style_check:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  script:
    - pycodestyle d3m

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

type_check:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  script:
    # We install the package to get dependencies which might contain typing information.
    - pip3 install .[tests]
    - pip3 install --requirement typing-requirements.txt
    - mypy d3m

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

tests:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  services:
    # We use our copy of docker:19-dind Docker image to not hit Docker Hub pull limits.
    - name: registry.gitlab.com/datadrivendiscovery/images/docker:19-dind
      alias: docker

  before_script:
    - python3 -c 'import docker; import json; print(json.dumps(docker.from_env().info(), indent=True))'

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_HOST: tcp://docker:2375

  script:
    - python3 setup.py sdist
    - pip3 install $(ls dist/*)[tests]
    - rm -rf d3m d3m.*
    - pip3 freeze
    - pip3 check
    - python3 ./run_tests.py

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

tests_windows:
  stage: build

  tags:
    - shared-windows
    - windows
    - windows-1809

  variables:
    GIT_SUBMODULE_STRATEGY: recursive

  before_script:
    - choco install python -y --version=$PYTHON_VERSION --no-progress
    - choco install sed -y --version=4.8 --no-progress
    - refreshenv
    - curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    - py get-pip.py
    - refreshenv
    - $env:Path += ";C:\Python36\Scripts;C:\Python37\Scripts;C:\Python38\Scripts"

  script:
    - py setup.py sdist
    - pip install "$(ls dist/*)[tests]"
    - rm -r -fo d3m,d3m.*
    - pip freeze
    - pip check
    - py run_tests.py

  parallel:
    matrix:
      - PYTHON_VERSION: ['3.6.8', '3.7.9', '3.8.10']

tests_oldest_dependencies:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  services:
    # We use our copy of docker:19-dind Docker image to not hit Docker Hub pull limits.
    - name: registry.gitlab.com/datadrivendiscovery/images/docker:19-dind
      alias: docker

  before_script:
    - python3 -c 'import docker; import json; print(json.dumps(docker.from_env().info(), indent=True))'

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_HOST: tcp://docker:2375

  script:
    - python3 setup.py sdist
    - pip3 install $(ls dist/*)[tests]
    - rm -rf d3m d3m.*
    # We first install just numpy because other packages compile against it and they have
    # to compile against the older version and not the latest currently installed.
    - ./oldest_dependencies.py | grep numpy | pip3 install --upgrade --upgrade-strategy only-if-needed --exists-action w --requirement /dev/stdin
    - ./oldest_dependencies.py | pip3 install --upgrade --upgrade-strategy only-if-needed --exists-action w --requirement /dev/stdin
    - pip3 freeze
    - pip3 check
    - python3 ./run_tests.py

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

tests_with_optimize:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  services:
    # We use our copy of docker:19-dind Docker image to not hit Docker Hub pull limits.
    - name: registry.gitlab.com/datadrivendiscovery/images/docker:19-dind
      alias: docker

  before_script:
    - python3 -c 'import docker; import json; print(json.dumps(docker.from_env().info(), indent=True))'

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_HOST: tcp://docker:2375
    PYTHONOPTIMIZE: "true"

  script:
    - python3 setup.py sdist
    - pip3 install $(ls dist/*)[tests]
    - rm -rf d3m d3m.*
    - pip3 freeze
    - pip3 check
    - python3 ./run_tests.py

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

tests_individually:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  services:
    - docker:dind

  before_script:
    - python3 -c 'import docker; import json; print(json.dumps(docker.from_env().info(), indent=True))'

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_HOST: tcp://docker:2375

  script:
    - python3 setup.py sdist
    - pip3 install $(ls dist/*)[tests]
    - rm -rf d3m d3m.*
    - pip3 freeze
    - pip3 check
    - |
      set -o errexit
      for FILE in tests/*.py; do echo "$FILE"; python3 "$FILE"; done

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

benchmarks:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  services:
    # We use our copy of docker:19-dind Docker image to not hit Docker Hub pull limits.
    - name: registry.gitlab.com/datadrivendiscovery/images/docker:19-dind
      alias: docker

  before_script:
    - python3 -c 'import docker; import json; print(json.dumps(docker.from_env().info(), indent=True))'

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_HOST: tcp://docker:2375

  script:
    - ./run_benchmarks.sh

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

test_datasets:
  stage: build

  tags:
    - datasets

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  services:
    # We use our copy of docker:19-dind Docker image to not hit Docker Hub pull limits.
    - name: registry.gitlab.com/datadrivendiscovery/images/docker:19-dind
      alias: docker

  before_script:
    - python3 -c 'import docker; import json; print(json.dumps(docker.from_env().info(), indent=True))'
    - git -C /data/datasets show -s
    - git -C /data/datasets_public show -s

  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    DOCKER_HOST: tcp://docker:2375

  script:
    - python3 setup.py sdist
    - pip3 install $(ls dist/*)[tests]
    - rm -rf d3m
    - find /data/datasets/ -name datasetDoc.json -print0 | xargs -r -0 python3 -m d3m dataset describe --continue --list --time
    - find /data/datasets_public/ -name datasetDoc.json -print0 | xargs -r -0 python3 -m d3m dataset describe --continue --list --time
    - find /data/datasets/ -name problemDoc.json -print0 | xargs -r -0 python3 -m d3m problem describe --continue --list --no-print
    - find /data/datasets_public/ -name problemDoc.json -print0 | xargs -r -0 python3 -m d3m problem describe --continue --list --no-print

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

run_check:
  stage: build

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:$IMAGE_TAG

  script:
    - python3 setup.py sdist
    - pip3 install $(ls dist/*)[tests]
    - rm -rf d3m d3m.*
    - python3 -m d3m --version

  parallel:
    matrix:
      - IMAGE_TAG: ['ubuntu-bionic-python36', 'ubuntu-bionic-python37', 'ubuntu-bionic-python38']

pages:
  stage: deploy

  image: registry.gitlab.com/datadrivendiscovery/images/testing-lite:ubuntu-bionic-python36

  script:
    - |
      set -o errexit
      if [[ ${CI_COMMIT_TAG} == v* ]] || [[ ${CI_COMMIT_REF_NAME} == devel ]]; then
        ./site/build_site.sh
        git checkout devel
        python3 site/build_site_types.py
      fi

  artifacts:
    paths:
      - public

  only:
    - devel
    - tags

trigger_images_rebuild_devel:
  stage: deploy

  image: registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic

  script:
    # This triggers a pipeline in https://gitlab.com/datadrivendiscovery/images
    - if [ -n "$TRIGGER_TOKEN" ]; then curl --fail -X POST -F token=$TRIGGER_TOKEN -F ref=master -F variables[REBUILD_IMAGE]=core:ubuntu-bionic-python36-devel,core:ubuntu-bionic-python37-devel,core:ubuntu-bionic-python38-devel,core-lite:ubuntu-bionic-python36-devel,core-lite:ubuntu-bionic-python37-devel,core-lite:ubuntu-bionic-python38-devel https://gitlab.com/api/v4/projects/5024100/trigger/pipeline; fi

  only:
    - devel

trigger_images_rebuild_master:
  stage: deploy

  image: registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic

  script:
    # This triggers a pipeline in https://gitlab.com/datadrivendiscovery/images
    - if [ -n "$TRIGGER_TOKEN" ]; then curl --fail -X POST -F token=$TRIGGER_TOKEN -F ref=master -F variables[REBUILD_IMAGE]=core:ubuntu-bionic-python36-master,core:ubuntu-bionic-python37-master,core:ubuntu-bionic-python38-master,core-lite:ubuntu-bionic-python36-master,core-lite:ubuntu-bionic-python37-master,core-lite:ubuntu-bionic-python38-master https://gitlab.com/api/v4/projects/5024100/trigger/pipeline; fi

  only:
    - master

trigger_docs_generation:
  stage: deploy

  image: registry.gitlab.com/datadrivendiscovery/images/testing-docker:ubuntu-bionic

  script:
    # This triggers a pipeline in https://gitlab.com/datadrivendiscovery/docs
    - if [ -n "$DOCS_TRIGGER_TOKEN" ]; then curl --fail -X POST -F token=$DOCS_TRIGGER_TOKEN -F ref=master https://gitlab.com/api/v4/projects/7130595/trigger/pipeline; fi

  only:
    - devel
    - tags
